~/.aliases.d
===
The aliases I use on my machine. To `source` these aliases in my `~/.zshrc` or `~/.bashrc` I added this for loop in an if statment:

```
if [[ -d ~/.aliases.d ]]; then
	for file in ~/.aliases.d/*.aliases ; do
		source $file
	done
fi
```
